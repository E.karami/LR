import numpy as np
import mpmath as mp

class logistic_regression:

    def __init__(self):
        self.details = None
        self.repeat = None
        self.theta = None

    def fit(self, x, y):
        xarray = np.array(x)
        yarray = np.array(y)
        ones = np.ones_like(x[:, 0])
        xarray = np.hstack((np.reshape(np.ones_like(x[:, 0]), (-1, 1)), x))
        theta = np.random.rand(xarray.shape[1], 1)
        yarray = y.reshape((-1, 1))
        dict = self.gradiant_descent(xarray, yarray, theta)
        self.theta = dict.get('theta')
        self.repeat = dict.get('repeat')
        self.details = dict.get('details')
        return self

    def gradiant_descent(self, x, y, theta):
        alpha = 0.001
        jdetails = np.array([])
        jrepeat = np.array([])
        last = self.cost_function(x, y, theta)
        index = 1
        while (True):
            theta = theta - alpha * (np.dot(x.T, (self.hypotsis_function(x, theta) - y)))
            jdetails = np.hstack((jdetails, np.array([self.cost_function(x, y, theta)])))
            #         print('\n\n\n\n\n\n')
            #         print(jdetails)
            jrepeat = np.hstack((jrepeat, np.array([index])))
            index += 1
            if self.cost_function(x, y, theta) >= last:
                alpha = alpha / 3
            elif last - self.cost_function(x, y, theta) < 0.001:
                return {'theta': theta, 'details': jdetails, 'repeat': jrepeat}
            last = self.cost_function(x, y, theta)

    def hypotsis_function(self, x, theta):
        mpvec = np.frompyfunc(mp.exp, 1, 1)
        result = 1 / (1 + mpvec(-x @ theta))
        return result.astype('float64')

    def cost_function(self, x, y, theta):
        xfirstpart = np.log(1 - self.hypotsis_function(x, theta))
        xsecondpart = np.log(self.hypotsis_function(x, theta))
        yfirstpart = 1 - y
        ysecondpart = y
        result = -yfirstpart.reshape(1, -1) @ xfirstpart - ysecondpart.reshape(1, -1) @ xsecondpart
        return result[0, 0]

    def predict(self, x):
        x = np.hstack((np.reshape(np.ones_like(x[:, 0]), (-1, 1)), x))
        return x @ self.theta

    def predict_proba(self, x):
        x = np.hstack((np.ones_like(x[:, 0].reshape(-1, 1)), x))
        v = x @ self.theta
        l = np.hstack((v, 1 - v))
        return l